module Env.Config exposing (..)

import Config.Type exposing (Config)

config : Config
config =
    { profile = "dev"
    , apiRoot = "http://localhost:8080"
    }
