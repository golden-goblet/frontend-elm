module Env.Config exposing (..)

import Config.Type exposing (Config)

config : Config
config =
    { profile = "prod"
    , apiRoot = "https://golden-goblet.herokuapp.com"
    }
