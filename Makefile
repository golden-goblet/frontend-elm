.PHONY: hs-to-elm build config

profile ?= dev
ELM_OUT = elm

hs-to-elm:
	cd ./haskell-to-elm \
	&& stack build haskell-to-elm \
	&& stack run -- haskell-to-elm

set-config:
	-rm -rf elm-stuff/
	@echo Using profile \"$(profile)\"
	ln -s -f -T '../config/$(profile)' src/Env

build: set-config
	elm make src/Main.elm --optimize --output $(ELM_OUT).js
	uglifyjs $(ELM_OUT).js --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output $(ELM_OUT).min.js
	rm $(ELM_OUT).js
	mv $(ELM_OUT).min.js ./public
