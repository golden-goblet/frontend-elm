module Config.Type exposing (..)

type alias Config =
    { profile : String
    , apiRoot : String
    }
