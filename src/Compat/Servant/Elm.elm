module Compat.Servant.Elm exposing (..)

import Time exposing (..)
import Json.Decode exposing (..)

import Iso8601

jsonDecPosix : Decoder Posix
jsonDecPosix = Iso8601.decoder

jsonEncPosix : Posix -> Value
jsonEncPosix = Iso8601.encode
