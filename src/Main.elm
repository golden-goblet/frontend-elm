module Main exposing (..)

import Api.Types exposing (..)

import List
import Result
import Tuple
import Array exposing (Array)
import Maybe
import Browser
import Html exposing (Html)
import Json.Decode
import Http
import Time

import Element exposing (..)
import Element.Font as Font
import Element.Background as Background

import Config.Type
import Env.Config as Env
import Api.Request as Api
import Api.Pagination exposing (Order(..))
import Page.DetailedEvent as DetailedEvent

main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }

type Model
    = DetailedEvent DetailedEvent.Model

type Msg
    = GotDetailedEventMsg DetailedEvent.Msg

updateWith : (subModel -> Model) -> (subMsg -> Msg) -> (subModel, Cmd subMsg) -> (Model, Cmd Msg)
updateWith toModel toMsg = Tuple.mapBoth toModel (Cmd.map toMsg)

init : () -> (Model, Cmd Msg)
init _ = updateWith DetailedEvent GotDetailedEventMsg <| DetailedEvent.init ()

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case (msg, model) of
        (GotDetailedEventMsg subMsg, DetailedEvent subModel) ->
            updateWith DetailedEvent GotDetailedEventMsg
                <| DetailedEvent.update subMsg subModel

view : Model -> Html Msg
view model =
    let
        viewPage toMsg subView =
            Html.map toMsg subView
    in
        case model of
            DetailedEvent subModel ->
                viewPage GotDetailedEventMsg <| DetailedEvent.view subModel
