module Api.Request exposing (..)

import Config.Type
import Env.Config as Env
import Api.Pagination as Pagination exposing (Pagination)
import Api.Types exposing (..)

import Iso8601

import Http
import Url
import Url.Builder as Url exposing (QueryParameter)
import Json.Decode as Json
import Dict
import Time

request :
    { extraHeaders : List Http.Header
    , timeout : Maybe Float
    , tracker : Maybe String
    , request : Request msg
    }
    -> Cmd msg
request config =
    let
        req = config.request
        httpRequest = case req.withCredentials of
            False -> Http.request
            True -> Http.riskyRequest
    in
        httpRequest
            { body = req.body
            , expect = req.expect
            , headers = config.extraHeaders ++ req.headers
            , method = req.method
            , timeout = config.timeout
            , tracker = config.tracker
            , url = req.url
            }


type alias Request msg =
    { body : Http.Body
    , expect : Http.Expect msg
    , headers : List Http.Header
    , method : String
    , url : String
    , withCredentials : Bool
    }

expectJsonResponse :
   (Result Http.Error (a, b) -> msg)
   -> Json.Decoder a
   -> (Http.Metadata -> b)
   -> Http.Expect msg
expectJsonResponse toMsg decoder mapMetadata =
  Http.expectStringResponse toMsg
  <| \response ->
      case response of
        Http.BadUrl_ url ->
          Err (Http.BadUrl url)

        Http.Timeout_ ->
          Err Http.Timeout

        Http.NetworkError_ ->
          Err Http.NetworkError

        Http.BadStatus_ metadata _ ->
          Err (Http.BadStatus metadata.statusCode)

        Http.GoodStatus_ metadata body ->
          case Json.decodeString decoder body of
            Ok value ->
              Ok (value, mapMetadata metadata)

            Err err ->
              Err (Http.BadBody (Json.errorToString err))

type Range a
    = NextRange String
    | CustomRange
        { field : String
        , value : Maybe a
        , offset : Maybe Int
        , limit : Maybe Int
        , order : Maybe Pagination.Order
        }

rangeToPagination : (a -> String) -> Range a -> Pagination
rangeToPagination toStr range = case range of
    NextRange rangeStr -> Pagination.NextRange rangeStr
    CustomRange {field, value, offset, limit, order} ->
        Pagination.CustomRange
        <| Pagination.Range field ((Maybe.map toStr) value) offset limit order

rangeToHeader : (a -> String) -> Range a -> Http.Header
rangeToHeader toStr range = Pagination.toHeader <| rangeToPagination toStr range

events :
    Maybe (Range Time.Posix)
    -> (Result Http.Error (List DetailedEvent, Maybe (Range Time.Posix)) -> msg)
    -> Request msg
events mrange toMsg =
    { method = "GET"
    , headers = Maybe.withDefault [] <| Maybe.map (List.singleton << rangeToHeader Iso8601.fromTime) mrange
    , withCredentials = False
    , url = Url.crossOrigin Env.config.apiRoot ["detailed-events"] []
    , body = Http.emptyBody
    , expect = expectJsonResponse
        toMsg
        (Json.list jsonDecDetailedEvent)
        (\metadata -> Maybe.map NextRange <| Dict.get "next-range" metadata.headers)
    }
