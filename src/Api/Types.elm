module Api.Types exposing(..)

import Json.Decode
import Json.Encode exposing (Value)
-- The following module comes from bartavelle/json-helpers
import Json.Helpers exposing (..)
import Dict exposing (Dict)
import Set
import Http
import String
import Url.Builder

import Time exposing (..)
import Compat.Servant.Elm exposing (..)

type alias DetailedEvent  =
   { event_id: Int
   , event_name: String
   , game_id: Int
   , game_name: String
   , starts_on: Posix
   , ends_on: Posix
   , matches_expected: Int
   , description: (Maybe String)
   , match_info: (List DetailedEventMatch)
   , contestant_entries: (List DetailedEventContestantEntry)
   }

jsonDecDetailedEvent : Json.Decode.Decoder ( DetailedEvent )
jsonDecDetailedEvent =
   Json.Decode.succeed (\pevent_id pevent_name pgame_id pgame_name pstarts_on pends_on pmatches_expected pdescription pmatch_info pcontestant_entries -> {event_id = pevent_id, event_name = pevent_name, game_id = pgame_id, game_name = pgame_name, starts_on = pstarts_on, ends_on = pends_on, matches_expected = pmatches_expected, description = pdescription, match_info = pmatch_info, contestant_entries = pcontestant_entries})
   |> required "event_id" (Json.Decode.int)
   |> required "event_name" (Json.Decode.string)
   |> required "game_id" (Json.Decode.int)
   |> required "game_name" (Json.Decode.string)
   |> required "starts_on" (jsonDecPosix)
   |> required "ends_on" (jsonDecPosix)
   |> required "matches_expected" (Json.Decode.int)
   |> fnullable "description" (Json.Decode.string)
   |> required "match_info" (Json.Decode.list (jsonDecDetailedEventMatch))
   |> required "contestant_entries" (Json.Decode.list (jsonDecDetailedEventContestantEntry))

jsonEncDetailedEvent : DetailedEvent -> Value
jsonEncDetailedEvent  val =
   Json.Encode.object
   [ ("event_id", Json.Encode.int val.event_id)
   , ("event_name", Json.Encode.string val.event_name)
   , ("game_id", Json.Encode.int val.game_id)
   , ("game_name", Json.Encode.string val.game_name)
   , ("starts_on", jsonEncPosix val.starts_on)
   , ("ends_on", jsonEncPosix val.ends_on)
   , ("matches_expected", Json.Encode.int val.matches_expected)
   , ("description", (maybeEncode (Json.Encode.string)) val.description)
   , ("match_info", (Json.Encode.list jsonEncDetailedEventMatch) val.match_info)
   , ("contestant_entries", (Json.Encode.list jsonEncDetailedEventContestantEntry) val.contestant_entries)
   ]



type alias DetailedEventMatch  =
   { match: Int
   , name: String
   }

jsonDecDetailedEventMatch : Json.Decode.Decoder ( DetailedEventMatch )
jsonDecDetailedEventMatch =
   Json.Decode.succeed (\pmatch pname -> {match = pmatch, name = pname})
   |> required "match" (Json.Decode.int)
   |> required "name" (Json.Decode.string)

jsonEncDetailedEventMatch : DetailedEventMatch -> Value
jsonEncDetailedEventMatch  val =
   Json.Encode.object
   [ ("match", Json.Encode.int val.match)
   , ("name", Json.Encode.string val.name)
   ]



type alias DetailedEventContestantEntry  =
   { contestant: Contestant
   , entries: (List DetailedEventEntry)
   }

jsonDecDetailedEventContestantEntry : Json.Decode.Decoder ( DetailedEventContestantEntry )
jsonDecDetailedEventContestantEntry =
   Json.Decode.succeed (\pcontestant pentries -> {contestant = pcontestant, entries = pentries})
   |> required "contestant" (jsonDecContestant)
   |> required "entries" (Json.Decode.list (jsonDecDetailedEventEntry))

jsonEncDetailedEventContestantEntry : DetailedEventContestantEntry -> Value
jsonEncDetailedEventContestantEntry  val =
   Json.Encode.object
   [ ("contestant", jsonEncContestant val.contestant)
   , ("entries", (Json.Encode.list jsonEncDetailedEventEntry) val.entries)
   ]



type alias DetailedEventEntry  =
   { match: Int
   , result: String
   , rank: (Maybe Int)
   , points: (Maybe Float)
   , note: (Maybe String)
   , youtube_video: (Maybe String)
   }

jsonDecDetailedEventEntry : Json.Decode.Decoder ( DetailedEventEntry )
jsonDecDetailedEventEntry =
   Json.Decode.succeed (\pmatch presult prank ppoints pnote pyoutube_video -> {match = pmatch, result = presult, rank = prank, points = ppoints, note = pnote, youtube_video = pyoutube_video})
   |> required "match" (Json.Decode.int)
   |> required "result" (Json.Decode.string)
   |> fnullable "rank" (Json.Decode.int)
   |> fnullable "points" (Json.Decode.float)
   |> fnullable "note" (Json.Decode.string)
   |> fnullable "youtube_video" (Json.Decode.string)

jsonEncDetailedEventEntry : DetailedEventEntry -> Value
jsonEncDetailedEventEntry  val =
   Json.Encode.object
   [ ("match", Json.Encode.int val.match)
   , ("result", Json.Encode.string val.result)
   , ("rank", (maybeEncode (Json.Encode.int)) val.rank)
   , ("points", (maybeEncode (Json.Encode.float)) val.points)
   , ("note", (maybeEncode (Json.Encode.string)) val.note)
   , ("youtube_video", (maybeEncode (Json.Encode.string)) val.youtube_video)
   ]



type alias Contestant  =
   { handle: String
   , name: (Maybe String)
   , youtube_channel: (Maybe String)
   }

jsonDecContestant : Json.Decode.Decoder ( Contestant )
jsonDecContestant =
   Json.Decode.succeed (\phandle pname pyoutube_channel -> {handle = phandle, name = pname, youtube_channel = pyoutube_channel})
   |> required "handle" (Json.Decode.string)
   |> fnullable "name" (Json.Decode.string)
   |> fnullable "youtube_channel" (Json.Decode.string)

jsonEncContestant : Contestant -> Value
jsonEncContestant  val =
   Json.Encode.object
   [ ("handle", Json.Encode.string val.handle)
   , ("name", (maybeEncode (Json.Encode.string)) val.name)
   , ("youtube_channel", (maybeEncode (Json.Encode.string)) val.youtube_channel)
   ]


