module Api.Pagination exposing
    ( Pagination(..)
    , Range
    , Order(..)
    , toHeader
    )

import String
import List
import Http
import Url

type Pagination
    = CustomRange Range
    | NextRange String

type alias Range =
    { field : String
    , value : Maybe String
    , offset : Maybe Int
    , limit : Maybe Int
    , order : Maybe Order
    }

type Order
    = Asc
    | Desc

orderToString : Order -> String
orderToString order = case order of
    Asc -> "asc"
    Desc -> "desc"

toHeader : Pagination -> Http.Header
toHeader page = case page of
    CustomRange range ->
        let
            fieldValue =
                List.singleton
                << String.join " "
                << (::) range.field
                << Maybe.withDefault []
                <| Maybe.map (List.singleton << Url.percentEncode) range.value
            argToList name f val = Maybe.withDefault [] <| Maybe.map (\v -> [name ++ " " ++ f v]) val
            offset = argToList "offset" String.fromInt range.offset
            limit  = argToList "limit"  String.fromInt range.limit
            order  = argToList "order"  orderToString range.order
            rangeStr = String.join "; " (fieldValue ++ offset ++ limit ++ order)
        in
            Http.header "Range" rangeStr
    NextRange rangeStr -> Http.header "Range" rangeStr
