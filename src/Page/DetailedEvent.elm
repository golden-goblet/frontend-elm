module Page.DetailedEvent exposing (..)

import Api.Types exposing (..)

import List
import Result
import Tuple
import Array exposing (Array)
import Maybe
import Browser
import Html exposing (Html)
import Json.Decode
import Http
import Time

import Element exposing (..)
import Element.Input as Input
import Element.Font as Font
import Element.Background as Background

import Config.Type
import Env.Config as Env
import Api.Request as Api
import Api.Pagination exposing (Order(..))

main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = always Sub.none
        , view = view
        }

type Model
    = Loading
    | Failure Http.Error
    | Success DetailedEvent (Maybe (Api.Range Time.Posix))

type Msg
    = GotDetailedEvent (Result Http.Error (List DetailedEvent, Maybe (Api.Range Time.Posix)))
    | NextRange

init : () -> (Model, Cmd Msg)
init _ =
    ( Loading
    , fetchModel Nothing
    )

fetchModel : Maybe (Api.Range Time.Posix) -> Cmd Msg
fetchModel mrange = Api.request
        { extraHeaders = []
        , timeout = Nothing
        , tracker = Nothing
        , request = Api.events mrange GotDetailedEvent
        }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case (msg, model) of
        (GotDetailedEvent (Ok ([detailedEvent], range)), _) -> (Success detailedEvent range, Cmd.none)
        (GotDetailedEvent (Ok _), _) -> (Failure (Http.BadBody "non singleton list in body"), Cmd.none)
        (GotDetailedEvent (Err httpError), _) -> (Failure httpError, Cmd.none)
        --GotDetailedEvent _ -> (Failure "", Cmd.none)

        (NextRange, Success _ mnextRange) -> (model, fetchModel mnextRange)

        (_, _) -> (model, Cmd.none)

view : Model -> Html Msg
view model =
    layout [] <| case model of
        Loading -> text "loading event"

        Failure (Http.BadUrl url) -> text ("Failed due to bad url " ++ url)
        Failure Http.Timeout -> text "Failed due to timeout."
        Failure Http.NetworkError -> text "Failed due to network error."
        Failure (Http.BadStatus status) -> text ("Failed due to bad status " ++ String.fromInt status)
        Failure (Http.BadBody decodeError) -> text ("Failed due to: " ++ decodeError)

        Success de range ->
            let
                (headers, matchRows) = detailedEventMatchesToRows de

                joinMaybe : Maybe (Maybe a) -> Maybe a
                joinMaybe ma =
                    case ma of
                        Just a -> a
                        Nothing -> Nothing

                viewHeader : Int -> Element Msg
                viewHeader colIndex =
                    Maybe.withDefault none
                    << Maybe.map (el [padding 5, Font.center] << text)
                    << Maybe.map .handle
                    <| Array.get colIndex headers

                rowOrDetailedEventMatchToString : Result Int DetailedEventMatch -> String
                rowOrDetailedEventMatchToString rowOrMatchInfo =
                    case rowOrMatchInfo of
                        Ok matchInfo -> matchInfo.name
                        Err row -> String.fromInt row

                viewRowHeader : DetailedEventRow -> Element Msg
                viewRowHeader row =
                    el [padding 5, Font.center]
                    << text
                    << rowOrDetailedEventMatchToString
                    <| row.rowHeader

                viewRow : Int -> DetailedEventRow -> Element Msg
                viewRow colIndex row =
                    Maybe.withDefault none
                    << Maybe.map entryToElement
                    << joinMaybe
                    << Array.get colIndex
                    <| row.entries

                entryToElement : DetailedEventEntry -> Element msg
                entryToElement dee =
                    let
                        resultElement = case dee.youtube_video of
                            Just videoUrl -> link [centerX] { url = videoUrl, label = text dee.result }
                            Nothing -> el [centerX] << text <| dee.result
                    in
                        el [padding 5] resultElement

                makeCols : Int -> Int -> List (Column (DetailedEventRow) Msg)
                makeCols colIndex numCols =
                    if colIndex < numCols
                    then
                        { header = viewHeader colIndex
                        , width = fill
                        , view = viewRow colIndex
                        } :: makeCols (colIndex + 1) numCols
                    else
                        []

                theCols =
                    { header = none
                    , width = fill
                    , view = viewRowHeader
                    } :: (makeCols 0 <| Array.length headers)
            in
                column []
                [ table [centerX, Background.color <| rgb255 100 200 120, padding 20 ]
                    { data = matchRows
                    , columns = theCols
                    }
                , Input.button []
                    { onPress = Just NextRange
                    , label = text "Next"
                    }
                ]

type alias DetailedEventRow =
    { rowHeader : Result Int DetailedEventMatch
    , entries : Array (Maybe DetailedEventEntry)
    }

detailedEventMatchesToRows : DetailedEvent -> (Array Contestant, List (DetailedEventRow))
detailedEventMatchesToRows de =
    let
        collectContestants = Array.fromList << List.map .contestant
        collectEntries contestantEntries =
            let
                heads row detailedEventEntries =
                    let
                        head entries = case entries of
                            (e :: es) ->
                                if row == e.match
                                then Just e
                                else Nothing
                            [] -> Nothing
                    in
                        List.map head detailedEventEntries

                tails row detailedEventEntries =
                    let
                        tail entries = case entries of
                            (e :: es) ->
                                if row < e.match
                                then entries
                                else es
                            [] -> []
                    in
                        List.map tail detailedEventEntries

                transpose : Int -> Int -> List DetailedEventMatch -> List (List DetailedEventEntry) -> List DetailedEventRow
                transpose row numRows matchInfo detailedEventEntries =
                    let
                        isJust m =
                            case m of
                                Just _ -> True
                                Nothing -> False
                        theHeads = heads row detailedEventEntries
                        theTails = tails row detailedEventEntries

                        (rowHeader, rowHeadersTail) = case matchInfo of
                            (mi :: mis) ->
                                if mi.match == row
                                then
                                    (Ok mi, mis)
                                else
                                    (Err row
                                    , if row < mi.match
                                        then matchInfo
                                        else mis -- probably should never happen
                                    )
                            [] -> (Err row, [])
                        detailedEventRow = DetailedEventRow rowHeader <| Array.fromList theHeads
                    in
                        if row <= numRows || List.any (isJust) theHeads
                        then detailedEventRow :: transpose (row + 1) numRows rowHeadersTail theTails
                        else []
            in
                transpose 1 de.matches_expected (de.match_info) (List.map .entries contestantEntries)

    in
        (collectContestants de.contestant_entries, collectEntries de.contestant_entries)


