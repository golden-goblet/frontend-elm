{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}

module Main where

import Data.Api
import Data.Entities
import Data.Responses

import Data.Proxy

import Data.Aeson
import Servant.API
import Elm.TyRep
import Elm.Module
import Servant.Elm

-- orphan instances
deriveElmDef detailedEventJsonOptions ''DetailedEvent
deriveElmDef detailedEventMatchJsonOptions ''DetailedEventMatch
deriveElmDef detailedEventContestantEntryJsonOptions ''DetailedEventContestantEntry
deriveElmDef detailedEventEntryJsonOptions ''DetailedEventEntry
deriveElmDef gameJsonOptions ''Game
deriveElmDef eventJsonOptions ''Event
deriveElmDef contestantJsonOptions ''Contestant

main :: IO ()
main = generateElmModuleWith
	defElmOptions
		{ -- urlPrefix = Static "http://localhost:8080"
		  elmTypeAlterations = customTypeAlterations . defaultTypeAlterations
		, elmAlterations = recAlterType customTypeAlterations . defaultAlterations
		}
	["Api", "Types"]
	( defElmImports
		<> "\nimport Time exposing (..)"
		<> "\nimport Compat.Servant.Elm exposing (..)"
		<> "\n"
	)
	"../src"
	[ DefineElm (Proxy @DetailedEvent)
	, DefineElm (Proxy @DetailedEventMatch)
	, DefineElm (Proxy @DetailedEventContestantEntry)
	, DefineElm (Proxy @DetailedEventEntry)
	, DefineElm (Proxy @Contestant)
	--, DefineElm (Proxy @(Key Event))
	]
	-- Genration of client is a little to literal
	-- and type families or types with type level lists such as Ranges
	-- do not naively convert to valid elm
	-- and likely need some custom alteration as unsuccesfully attempted below.
	-- We could drop the servant-elm and use only elm-bridge
	-- but the simple single function for elm genration is nice
	-- and maybe generation of the client will be attempted again later.
	(Proxy @EmptyAPI)

customTypeAlterations :: EType -> EType
customTypeAlterations = \case
	ETyCon (ETCon {tc_name = "Int32"}) -> ETyCon (ETCon {tc_name = "Int"})
	ETyCon (ETCon {tc_name = "GameId"}) -> ETyCon (ETCon {tc_name = "Int"})
	ETyCon (ETCon {tc_name = "EventId"}) -> ETyCon (ETCon {tc_name = "Int"})
	-- >> print $ toElmType (Proxy @(Ranges '["end_time"] DetailedEvent))
	ETyApp (ETyApp (ETyCon (ETCon {tc_name = "Ranges"})) (ETyApp (ETyApp (ETyCon (ETCon {tc_name = "':"})) (ETyCon (ETCon {tc_name = "\"end_time\""}))) (ETyCon (ETCon {tc_name = "'[]"})))) (ETyCon (ETCon {tc_name = "DetailedEvent"}))
		-> ETyCon $ ETCon "RangeEndTime"
	t -> t
